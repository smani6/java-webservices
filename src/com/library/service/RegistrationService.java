package com.library.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.library.beans.User;
import com.library.dao.UserDAO;



@Path("/register")
public class RegistrationService {

    UserDAO dao = new UserDAO();
	
	@POST
	@Path("/add")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ResponseBuilder create(User user) {
		System.out.println(user.getUsername());
		System.out.println("creating user account");
		dao.create(user);
		return Response.status(Response.Status.OK);
		
	}
	
	
	
		
 
}
