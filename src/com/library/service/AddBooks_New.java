package com.library.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.library.beans.AddBooks;
import com.library.beans.Session;
import com.library.dao.AddAuthorDAO;
import com.library.dao.AddBooksDAO;
import com.library.dao.MockDAO;

@Path("/addbooks")
public class AddBooks_New {

	AddBooksDAO booksdao = new AddBooksDAO();
	
	/*@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AddBooks> findAll() {
		System.out.println("findAll");
		return booksdao.findAll();
	}*/

	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Status create(AddBooks addbooks) {
		System.out.println("creating Book");
		
		Session sessionbean = Session.getInstance();
	      
		// int userid = 1; // This user id have to get from db for username

		AddAuthorDAO authorDao = new AddAuthorDAO();
		
		int authorid = authorDao.create(addbooks.getAuthor());
		
	    AddBooks booksbean = booksdao.create(addbooks,sessionbean.getUsername(),sessionbean.getUserid(), authorid);
	    int bookid = booksbean.getId();
	    
	    // Inserting into author table
	    
	    
		return Response.Status.ACCEPTED;
	}


}
