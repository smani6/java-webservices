package com.library.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.library.beans.AddBooks;
import com.library.beans.Session;
import com.library.dao.AddAuthorDAO;
import com.library.dao.AddBooksDAO;
import com.library.dao.AddUserPreferencesDAO;


@Path("/addUserPreferences")
public class AddUserPreferences {
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Status create(AddBooks addbooks) {
		System.out.println("Adding userpreferences");
		
		Session sessionbean = Session.getInstance();
	      
		// int userid = 1; // This user id have to get from db for username

		AddAuthorDAO authorDao = new AddAuthorDAO();
		AddBooksDAO booksDao = new AddBooksDAO();
		AddUserPreferencesDAO preDao = new AddUserPreferencesDAO();
		
		
	    int userid = sessionbean.getUserid();
	    ///String bookname = addbooks.getBookname();
	    
	    //System.out.println("Bookname -------------" + bookname);
	    int bookid = addbooks.getId();
	    
	    System.out.println("Book-id-------------------" + bookid);
	    System.out.println("Bookname--------" + addbooks.getBookname());
	    // Inserting into author table
	    
	    preDao.create(userid, bookid, 1);
	    
		return Response.Status.ACCEPTED;
	}

}
