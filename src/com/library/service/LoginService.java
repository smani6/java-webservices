package com.library.service;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.internal.util.Base64;

import com.library.beans.LoginForm;
import com.library.beans.User;
import com.library.dao.AuthenticateDAO;
import com.library.dao.UserDAO;
import javax.xml.bind.DatatypeConverter;

@Path("/login")
public class LoginService {
		
		AuthenticateDAO a= new AuthenticateDAO();
		LoginForm loginbean = new LoginForm();
		
		@POST
		@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public Map  create(LoginForm login) {
			//System.out.println("In Login Service");
			//System.out.println(login.getUsername());
			//System.out.println(login.getPassword());
			
			// Get username and password from database for the username got from login form
			loginbean = a.match(login.getUsername());
			
			if ( null != loginbean.getUsername() && loginbean.getUsername().equals(login.getUsername()))
			{
				if (null != loginbean.getPassword()  && loginbean.getPassword().equals(login.getPassword()))
			
				{	
					Map response = new HashMap<>();
					// setting token
					String token = loginbean.getUsername();
					byte[] encodedBytes = Base64.encode(token.getBytes());
					System.out.println("In Login Service");
					System.out.println(new String(encodedBytes));
					
					int userid = loginbean.getId();
					String username = loginbean.getUsername();
					String token_value = new String(encodedBytes);
					
			        response.put("username", loginbean.getUsername());
			        response.put("token", encodedBytes);
			        
			        a.addToken(userid,username,token_value);
			        // return username and token to client
					return  response ;
				}
				else
					return null;
			}
			else
				return null;
						
		}
}
