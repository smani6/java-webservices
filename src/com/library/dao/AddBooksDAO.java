package com.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.library.beans.AddBooks;
import com.library.connectionHelper.NewConnectionHelper;;


public class AddBooksDAO {

	public List<AddBooks> findAll(int userid) {
        List<AddBooks> list = new ArrayList<AddBooks>();
        Connection c = null;
        PreparedStatement ps = null;
        System.out.println("AddBooksDAO userid    " + userid);
        List bookids = new ArrayList();
    	String sql = "select books.id,bookname,isbn13,isbn10,pubdate,description,pages,authorname from books join author on books.authorid = author.id where books.userid = ?";
        try {
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement(sql);
            ps.setInt(1,userid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            	int bookid = rs.getInt("id");
            	int want_to_read = get_user_preferences_of_a_book(bookid);
                list.add(processRow(rs,want_to_read));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        
        
        return list;
    }
	
	public int get_user_preferences_of_a_book(int bookid)
	{
		
        Connection c = null;
        PreparedStatement ps = null;
        System.out.println("Bookid    " + bookid);
        int want_to_read = 0;
        String sql = "select want_to_read from user_preferences where bookid = ?";
      
        try {
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement(sql);
            ps.setInt(1,bookid);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            	want_to_read = rs.getInt("want_to_read");
            
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        
		return want_to_read;
	}
	
	
	public static String preparePlaceHolders(int length) {
	    StringBuilder builder = new StringBuilder(length * 2 - 1);
	    for (int i = 0; i < length; i++) {
	        if (i > 0) builder.append(',');
	        builder.append('?');
	    }
	    return builder.toString();
	}

	public static void setValues(PreparedStatement preparedStatement, Object... values) throws SQLException {
	    for (int i = 0; i < values.length; i++) {
	        preparedStatement.setObject(i + 1, values[i]);
	    }
	}
	
	public Map get_user_preferences(List bookid)
	{
		List want_to_read = new ArrayList();
        Connection c = null;
        PreparedStatement ps = null;
        System.out.println("AddBooksDAO userid    " + bookid);
        Map m = new HashMap();
        String SQL_FIND = "select bookid,want_to_read from user_preferences where bookid in (%s)";
        
        String sql = String.format(SQL_FIND, preparePlaceHolders(bookid.size()));
        
        try {
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement(sql);
            setValues(ps, bookid.toArray());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            	
            	m.put(rs.getString("bookid"), rs.getString("want_to_read"));
            	
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        
		return m;
	}
	
    public AddBooks create(AddBooks addbooks, String username, int userid , int authorid) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
        	
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO books (bookname,isbn13, isbn10,pubdate, description, pages, userid, imgid,authorid) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)",new String[] { "ID" });
            ps.setString(1, addbooks.getBookname());
            ps.setString(2, addbooks.getIsbn13());
            ps.setString(3, addbooks.getIsbn10());
            ps.setString(4, addbooks.getPubdate());
            ps.setString(5, addbooks.getDescription());
            ps.setString(6, addbooks.getPages());
            ps.setInt(7, userid);
            ps.setInt(8,1);
            ps.setInt(9, authorid);
        
            
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            addbooks.setId(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        return addbooks;
    }

    protected AddBooks processRow(ResultSet rs,int want_to_read) throws SQLException {
        AddBooks Book = new AddBooks();
        Book.setId(rs.getInt("id"));
        Book.setBookname(rs.getString("bookname"));
        Book.setAuthor(rs.getString("authorname"));
        Book.setIsbn13(rs.getString("isbn13"));
        Book.setIsbn10(rs.getString("isbn10"));
        Book.setDescription(rs.getString("description"));
        Book.setPages(rs.getString("pages"));
        Book.setPubdate(rs.getString("pubdate"));
        Book.setWant_to_read(want_to_read);

        return Book;
    }
    
    
    public int getBookdId(String bookname)
    {
    	String sql = "select id from books where bookname = ?";
        Connection c = null;
        PreparedStatement ps = null;
        List ls = new ArrayList();
        int bookid =0;
        try {
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement(sql);
            ps.setString(1,bookname);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
            	bookid = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        return bookid;
    }
}
