package com.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jersey.repackaged.com.google.common.collect.Sets;

import com.library.beans.LoginForm;
import com.library.connectionHelper.NewConnectionHelper;

public class AuthenticateDAO {

	public LoginForm match(String username){
		
		LoginForm match = new LoginForm();
		String sql = "SELECT id, username, password from user WHERE username = ?";
        Connection c = null;
        try {
            c = NewConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
            	match.setId(rs.getInt("id"));
                match.setUsername(rs.getString("username"));
                match.setPassword(rs.getString("password"));         
            }
        } catch (Exception e) {
            e.printStackTrace(); 
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        return match;
		
		
	}
	
	public void addToken(int userid, String username, String token){
		
		String sql = "insert into tmp_token (userid, username,token) values (?,?,?)";
        Connection c = null;
        try {
            c = NewConnectionHelper.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1,userid);
            ps.setString(2, username);
            ps.setString(3, token);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            System.out.println("Token inserted id " + id);
        } catch (Exception e) {
            e.printStackTrace(); 
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
	}
	
	public List getToken(String username)
	{
		String sql = "select userid, username, token from tmp_token where username = ?";
        Connection c = null;
        PreparedStatement ps = null;
        List ls = new ArrayList();
        try {
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement(sql);
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
            	ls.add(rs.getInt("userid"));
                ls.add(rs.getString("username"));
                ls.add(rs.getString("token"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
        
        return ls;
	}
}
	
	
	
	

