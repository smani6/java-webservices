package com.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.library.connectionHelper.NewConnectionHelper;

public class AddUserPreferencesDAO {

	
	public int create(int userid, int bookid, int wantToRead ) {
        Connection c = null;
        PreparedStatement ps = null;
        int id = 0;
        try {
        	
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO user_preferences (userid,bookid,want_to_read) VALUES (?,?,?)",new String[] { "ID" });
            ps.setInt(1, userid);
            ps.setInt(2, bookid);
            ps.setInt(3, wantToRead);
        
            
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            id = rs.getInt(1);
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
       
        return id;
    }
	
}
