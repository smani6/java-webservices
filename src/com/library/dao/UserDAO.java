package com.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.library.connectionHelper.NewConnectionHelper;
import com.library.beans.User;

public class UserDAO {

	 public User create(User user) {
	        Connection c = null;
	        PreparedStatement ps = null;
	        try {
	        	
	    
	            c = NewConnectionHelper.getConnection();
	            ps = c.prepareStatement("INSERT INTO user (firstname, lastname, username, emailId, password) VALUES (?, ?, ?, ?,?)",new String[] { "ID" });
	            ps.setString(1, user.getFirstname());
	            ps.setString(2, user.getLastname());
	            ps.setString(3, user.getUsername());
	            ps.setString(4, user.getEmailId());
	            ps.setString(5, user.getPassword());
	            ps.executeUpdate();
	            ResultSet rs = ps.getGeneratedKeys();
	            rs.next();
	            // Update the id in the returned object. This is important as this value must be returned to the client.
	            int id = rs.getInt(1);
	            user.setId(id);
	        } catch (Exception e) {
	            e.printStackTrace();
	            throw new RuntimeException(e);
			} finally {
				NewConnectionHelper.close(c);
			}
	        return user;
	    }
	
}
