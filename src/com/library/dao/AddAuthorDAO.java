package com.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.library.beans.AddBooks;
import com.library.connectionHelper.NewConnectionHelper;

public class AddAuthorDAO {

	public int create(String author ) {
        Connection c = null;
        PreparedStatement ps = null;
        int id = 0;
        try {
        	
            c = NewConnectionHelper.getConnection();
            ps = c.prepareStatement("INSERT INTO author (authorname) VALUES (?)",new String[] { "ID" });
            ps.setString(1, author);
        
            
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            id = rs.getInt(1);
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			NewConnectionHelper.close(c);
		}
       
        return id;
    }
	
}
