package com.library.dao;

import com.library.beans.Session;


import java.util.ArrayList;
import java.util.List;

public class MockDAO {
    private static MockDAO instance = new MockDAO();
   
    private List<Session> sessions = new ArrayList<>();

    private MockDAO() {
        

        sessions.add(new Session("admin", "1234"));
       
    }

    public static MockDAO getInstance() {
        return instance;
    }

   

    public List<Session> getSessions() {
        return sessions;
    }
}
