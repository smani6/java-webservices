package com.library.newfilter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

@Provider
public class AuthFilter implements ContainerRequestFilter {
	public static final String AUTHENTICATION_HEADER = "Authorization";
	public static final String USERNAME = "username";

	@Override
	public void filter(ContainerRequestContext containerRequest)
			throws WebApplicationException {

		System.out.println("In AuthFilter implements ContainerRequestFilter ");
		
		UriInfo path = containerRequest.getUriInfo();
		System.out.println("Path " + path.getPath());
		
		if (path.getPath().startsWith("login")) {
			System.out.println("In AuthFilter implements ContainerRequestFilter For login");
		} 
		else {
		   
		System.out.println( "In AuthFilter for resources other than login");
		
		
		System.out.println(containerRequest.getHeaders().toString());
		
		//String contentType = containerRequest.getHeaderString(HttpHeaders.CONTENT_TYPE);
		
		//System.out.println("contentType" + contentType);
		
		//System.out.println(containerRequest.getHeaderString("auth_token"));
		
		System.out.println("Authorization" + containerRequest.getHeaderString("Authorization"));
		
		String authCredentials = containerRequest
				.getHeaderString(AUTHENTICATION_HEADER);

		System.out.println("authCredentials " + authCredentials);
		
		String username = containerRequest
				.getHeaderString(USERNAME);
		// better injected
		AuthenticationService authenticationService = new AuthenticationService();

		boolean authenticationStatus = authenticationService
				.authenticate(authCredentials,username);

		if (!authenticationStatus) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		 }
		}
	}
}