package com.library.newfilter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.ContainerResponse;

@Provider
@PreMatching
public class DemoRESTResponseFilter implements ContainerResponseFilter {

    private final static Logger log = Logger.getLogger( DemoRESTResponseFilter.class.getName() );

    @Override
    public void filter( ContainerRequestContext requestCtx, ContainerResponseContext responseCtx ) throws IOException {

    	System.out.println("In DemoRESTResponseFilter");
    	
        log.info( "Filtering REST Response" );

        String origin = requestCtx.getHeaderString("origin");
        //System.out.println(origin);
        //System.out.println(requestCtx.getHeaders());
        responseCtx.getHeaders().add( "Access-Control-Allow-Origin", origin );    // You may further limit certain client IPs with Access-Control-Allow-Origin instead of '*'
        responseCtx.getHeaders().add( "Access-Control-Allow-Credentials", "true" );
        responseCtx.getHeaders().add( "Access-Control-Allow-Methods", "GET, POST, DELETE, PUT" );
        responseCtx.getHeaders().add( "Access-Control-Allow-Headers", "auth_token");
        responseCtx.getHeaders().add( "Access-Control-Allow-Headers", "auth_token, Origin, X-Requested-With, Content-Type,username, Accept,Authorization");
  
       if (requestCtx.getMethod().equals("OPTIONS")){
    	   responseCtx.setStatus(200);
       }
    }
}