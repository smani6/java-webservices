package com.library.newfilter;
import javax.xml.bind.DatatypeConverter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.glassfish.jersey.internal.util.Base64;

import com.library.beans.Session;
import com.library.dao.AuthenticateDAO;
import com.library.dao.MockDAO;

public class AuthenticationService {
	public boolean authenticate(String authCredentials, String username) {
		
		AuthenticateDAO dao = new AuthenticateDAO();
		MockDAO mockdao = MockDAO.getInstance();

		Session sessionbean = Session.getInstance();
		
		if (null == authCredentials)
			return false;
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic"+ " ", "");
		
		System.out.println("encodedUserToken-------" + encodedUserPassword);
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.decode(encodedUserPassword.getBytes());
			System.out.println("decodedBytes-----------" + decodedBytes);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
			System.out.println("usernameAndPassword--------" + usernameAndPassword);
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(
				usernameAndPassword, ":");
		
		System.out.println("tokenizer--------" + tokenizer);
		
		final String password = tokenizer.nextToken();
		System.out.println("Decoding token" + new String(password));

		// we have fixed the userid and password as admin
		// call some UserService/LDAP here
		
		List ls = dao.getToken(username);
		/*boolean authenticationStatus = "admin".equals(username)
				&& "YWRtaW4=".equals(password);*/
		
		boolean authenticationStatus = ls.get(1).equals(username)
				&& ls.get(2).equals(password);
		
		sessionbean.setUserid((int) ls.get(0));
		sessionbean.setUsername(username);
		sessionbean.setToken(password);
		
		System.out.println("Returning authenticationStatus" + authenticationStatus);
		return authenticationStatus;
	}
}