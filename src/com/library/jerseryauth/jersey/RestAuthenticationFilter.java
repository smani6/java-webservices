package com.library.jerseryauth.jersey;


import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

public class RestAuthenticationFilter implements javax.servlet.Filter {
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filter) throws IOException, ServletException {
		
		System.out.println("In RestAuthentictionFilter");
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			 Enumeration<String> contentType = httpServletRequest.getHeaders(HttpHeaders.CONTENT_TYPE);
			 
			 while(contentType.hasMoreElements()) {
		            String headerName = (String)contentType.nextElement();
		            System.out.println(headerName + " = " + httpServletRequest.getHeader(headerName));
		        }
			
			 System.out.println("contentType" + contentType.toString().toString());
			 String authorization = httpServletRequest.getHeader("auth-token");
			 System.out.println("authorization" + authorization);
			 
			//System.out.println(httpServletRequest);
			/*System.out.println(" \n\n Headers");

	        Enumeration headerNames = httpServletRequest.getHeaderNames();
	        while(headerNames.hasMoreElements()) {
	            String headerName = (String)headerNames.nextElement();
	            System.out.println(headerName + " = " + httpServletRequest.getHeader(headerName));
	        }

	        System.out.println("\n\nParameters");

	        Enumeration params = httpServletRequest.getParameterNames();
	        while(params.hasMoreElements()){
	            String paramName = (String)params.nextElement();
	            System.out.println(paramName + " = " + httpServletRequest.getParameter(paramName));
	        }

			
			 System.out.println("\n\n Row data");
			 System.out.println(extractPostRequestBody(httpServletRequest));*/
			
			String authCredentials = httpServletRequest.getHeader(AUTHENTICATION_HEADER);

			System.out.println(authCredentials);
			// better injected
			AuthenticationService authenticationService = new AuthenticationService();

			boolean authenticationStatus = authenticationService
					.authenticate(authCredentials);

			if (authenticationStatus) {
				System.out.println("authenticationStatus" + authenticationStatus);
				filter.doFilter(request, response);
			} else {
				if (response instanceof HttpServletResponse) {
					HttpServletResponse httpServletResponse = (HttpServletResponse) response;
					httpServletResponse
							.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}
		}
	}

	static String extractPostRequestBody(HttpServletRequest request) {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = null;
            try {
                s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }
	
	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
