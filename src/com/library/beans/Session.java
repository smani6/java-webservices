package com.library.beans;


import java.security.Principal;

import com.library.dao.MockDAO;

public class Session implements Principal {
	
	private static Session instance = new Session();
    private String username, token;
    private int userid;

    public static Session getInstance() {
        return instance;
    }
    
    private Session() {
    }

    public Session(String username, String token) {
        this.setUsername(username);
        this.token = token;
    }

    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
